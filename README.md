# Docker Terraform

This project generates a Docker image from a dynamic `Dockerfile`. GitLab CI/CD is the responsible to run a pipeline everytime a tag is added to the repo.
The Docker image generated is based on `cern/alma9-base` (CERN Alma Linux 9) and it is added the Terrafom binary on top of it.
