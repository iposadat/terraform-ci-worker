FROM cern/alma9-base:20230111-2

# Terraform version label
LABEL ch.cern.terraform.version=1.4.0

# Terraform version env variabled
ENV TERRAFORM_VERSION=1.4.0

# Set SHELL env variable
ENV SHELL=/bin/bash

# Install EPEL
RUN dnf install -y epel-release

# Update packages
# Install git, openssh and unzip
# Install OpenStack CLI (python3 packages)
# Install tbag (trustedbag-client)
# Remove cache data
RUN dnf update -y && \
    dnf install -y \
    --nogpgcheck \
    --repofrompath='cloud9s-openstack-zed,http://linuxsoft.cern.ch/cern/centos/s9/cloud/$basearch/openstack-zed' \
    --repofrompath='config9al,http://linuxsoft.cern.ch/internal/repos/config9al-stable/$basearch/os' \
    git \
    openssh \
    python3-cinderclient \
    python3-heatclient \
    python3-magnumclient \
    python3-openstackclient \
    python3-requests-gssapi \
    trustedbag-client \
    unzip && \
    dnf clean all && \
    rm -rf /var/cache/dnf

# Download terrafom zip file and SHA256SUMS for Terraform
# Check Terrafom binary SHA256 checksum
# Unzip terraform zip file
# Move terraform binary to /usr/local/bin
# Remove terraform zip file
RUN curl -O "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip" && \
    curl -O "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_SHA256SUMS" && \
    sha256sum --check terraform_${TERRAFORM_VERSION}_SHA256SUMS 2>&1 | grep OK && \
    unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    mv terraform /usr/local/bin && \
    rm terraform_${TERRAFORM_VERSION}_linux_amd64.zip

# Entrypoint
ENTRYPOINT ["/usr/local/bin/terraform"]
